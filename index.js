let hour = document.querySelector("#hour")
let minut = document.querySelector("#minute")
let second = document.querySelector("#second")

setInterval(() => {
 
  second.textContent = new Date().getSeconds()
}, new Date().getSeconds());

setInterval(() => {
 
  minut.textContent = new Date().getMinutes()
}, new Date().getMinutes());

setInterval(() => {
 
  hour.textContent = new Date().getHours()
}, new Date().getHours());